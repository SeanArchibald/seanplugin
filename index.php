<?php
/*
Plugin Name: Test plugin ever
Plugin URI:  https://developer.wordpress.org/plugins/the-basics/
Description: Sean made this
Version:     20160911
Author:      Sean A
Author URI:  https://developer.wordpress.org/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wporg
Domain Path: /languages
*/

function sean_test_plugin() {
  return "<p>Test plugin ever!</p>";
}
add_shortcode( 'test', 'sean_test_plugin' );


// Register Custom Post Type
function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Albums', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Album', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Albums', 'text_domain' ),
		'name_admin_bar'        => __( 'Album', 'text_domain' ),
		'archives'              => __( 'Album Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Album:', 'text_domain' ),
		'all_items'             => __( 'All Albums', 'text_domain' ),
		'add_new_item'          => __( 'Add New Album', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Album', 'text_domain' ),
		'edit_item'             => __( 'Edit Album', 'text_domain' ),
		'update_item'           => __( 'Update Album', 'text_domain' ),
		'view_item'             => __( 'View Album', 'text_domain' ),
		'search_items'          => __( 'Search Album', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into album', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this album', 'text_domain' ),
		'items_list'            => __( 'Albums list', 'text_domain' ),
		'items_list_navigation' => __( 'Albums list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter albums list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Post Type', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'album', $args );

}
add_action( 'init', 'custom_post_type', 0 );


?>
